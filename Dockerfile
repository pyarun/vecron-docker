FROM node:8.11-slim
LABEL maintainer="Arun Mittal - mittal.talk@gmail.com"

ARG PORT=80 
ARG HOSTNAME=${HOSTNAME}

ENV DOCKERIZE_VERSION=v0.6.1 \
  HOSTNAME=${HOSTNAME} \
  PORT=${PORT} \
  EMAIL_FROM=mittal.talk@gmail.com \
  SMTP_HOSTNAME=10.253.1.76


RUN \
  ## install dockerize
  wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
  && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
  && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
  ## install Cronicle
  && curl -s https://raw.githubusercontent.com/pyarun/Cronicle/master/bin/install.js | node 

COPY src/ /opt/cronicle/ 

RUN cd /opt/cronicle && chmod +x -R bin/*.sh

WORKDIR /opt/cronicle/

EXPOSE ${PORT}

VOLUME [ "/opt/cronicle/conf" ]

CMD [ "sh", "/opt/cronicle/bin/server.sh", "master" ]
